using System;
using Xunit;
using dotnetcore;
using System.Linq;

namespace xUnitTestFordotnetcore {
    public class BasicTest {
        [Fact]
        public void InicialTest() {
            Assert.True(true);
        }
        [Fact]
        public void TestFirstName() {
            var contact = new Contact() { 
                FirstName ="Ingrid"
            };
            Assert.Equal("Ingrid",contact.FirstName);
        }
        [Fact]
        public void TestID() {
            var contact = new Contact() {
                ID = 1
            };
            Assert.Equal(1,contact.ID);
        }
        [Fact]
        public void TestLastName() {
            var contact = new Contact() {
                LastName = "Acosta"
            };
            Assert.Equal("Acosta",contact.LastName);
        }
        [Fact]
        public void TestAge() {
            var contact = new Contact() {
                Age = 100
            };
            Assert.Equal(100,contact.Age);
        }
        [Fact]
        public void TestNote() {
            var contact = new Contact() {
                Note = "Una nota muy grande para esta persona"
            };
            Assert.Equal("Una nota muy grande para esta persona",contact.Note);
        }
        [Fact]
        public void TestPhoneNumber() {
            var contact = new Contact() {
                PhoneNumber = "123456789"
            };
            Assert.Equal("123456789",contact.PhoneNumber);
        }
        [Fact]
        public void TestContactDataLoad() {
            IContacManager contactManager;
            contactManager = new ContactManager();
            contactManager.LoadContacs(new ContactDataStore().contacts);
            var contactExpected = new Contact {
                ID = 1
                        ,FirstName = "Alyira"
                        ,LastName = "Rodríguez"
                        ,Age = 45
                        ,Note = "COORDINADORA"
                        ,PhoneNumber = "02122083811"

            };
            Contact contactresult = contactManager.GetContact(contactExpected.ID );
            Assert.Equal(contactresult.ID,contactExpected.ID);
            Assert.Equal(contactresult.FirstName,contactExpected.FirstName);
            Assert.Equal(contactresult.LastName,contactExpected.LastName);
            Assert.Equal(contactresult.Age,contactExpected.Age);
            Assert.Equal(contactresult.Note,contactExpected.Note);
            Assert.Equal(contactresult.PhoneNumber,contactExpected.PhoneNumber);
            //Assert.Equal(contactExpected, contactresult);
        }
    }
}
