﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dotnetcore {
    public class ContactDataStore {
        public List<Contact> contacts { get; set; }
        public ContactDataStore() {
            contacts = new List<Contact> {
                    new Contact {
                        ID = 1
                        , FirstName= "Alyira"
                        , MidleName= "Yazmin"
                        , LastName = "Rodríguez"
                        , Age = 45
                        , Note = "COORDINADORA"
                        , PhoneNumber="02122083811"
                        , Email="alyira.rodriguez@galac.com"

                    }
                    , new Contact {
                        ID = 2
                        , FirstName= "Mayelin"
                        , MidleName= "Vanesa"
                        , LastName = "Sánchez"
                        , Age = 44
                        , Note = "DBA"
                        ,PhoneNumber =""
                        , Email=""
                    }
                    , new Contact {
                        ID = 3
                        , FirstName= "YUDITH"
                        , MidleName = "MARGARITA"
                        , LastName = "MARRERO"
                        , Age = 41
                        , Note = "PROGRAMADORA"
                        , PhoneNumber = ""
                        , Email="yudith.marrero@galac.com"
                    }

                };
        }

    }
}
