﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dotnetcore {
    public interface IContacManager {
        void LoadContacs(List<Contact> contacts);
        Contact GetContact(int ID);
        Contact GetLastContact();
    }
}
