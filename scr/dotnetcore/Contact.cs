﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dotnetcore {
    public class Contact {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string MidleName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public string Note { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
    }
}
