﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace dotnetcore
{
    class Program {
        private static Contact contact;
        private static IContacManager contactManager;
        static void Main(string [] args) {
            Console.WriteLine("======================================");
            Console.WriteLine("Bienvenidos al Sistema de Contactos!");
            Console.WriteLine("            VERSIÓN " + GetCurrentVersion() );
            Console.WriteLine("======================================");
            Console.WriteLine("");
            contactManager = new ContactManager();
            contactManager.LoadContacs(new ContactDataStore().contacts);
            string selectedMenu = "0";
            while (selectedMenu != "3") {
                Console.WriteLine("OPCIONES DEL SISTEMA:");
                Console.WriteLine("1.- BUSCAR POR ID (A)");
                Console.WriteLine("2.- OBTENER ÚLTIMO CONTACTO (B)");
                Console.WriteLine("3.- SALIR DEL SISTEMA (Q)");
                selectedMenu = Console.ReadLine();
                switch (selectedMenu.ToString()) {
                    case "1":
                    case "A":
                    case "a":
                        BuscaContacto();
                        break;

                    case "2":
                    case "B":
                    case "b":
                        BuscaUltimoContacto();
                        break;

                    case "3":
                    case "!":
                    case "Q":

                        break;
                    default:
                        break;
                }
            }

        }

        private static void BuscaContacto() {
            Console.Write("Ingrese un ID  de Contactos : ");
            string ID = Console.ReadLine();
            contact = contactManager.GetContact(Convert.ToInt32(ID));
            Console.WriteLine("Contacto Buscado: " + contact.FirstName + " " + contact.LastName + " Edad: " + contact.Age.ToString());
            Console.WriteLine("Nota: " + contact.Note);
            Console.ReadKey();
        }
        private static void BuscaUltimoContacto() {
            Console.WriteLine("");
            Console.WriteLine("Último Contacto: ");
            contact = contactManager.GetLastContact();
            Console.WriteLine("Contacto: " + contact.FirstName + " " + contact.MidleName + " " + contact.LastName + " Edad: " + contact.Age.ToString() + " Email: " + contact.Email);
            Console.ReadKey();
        }

        private static string  GetCurrentVersion() {
            return new ProgramVersion().CurrentVersion;
        }
    }
}
