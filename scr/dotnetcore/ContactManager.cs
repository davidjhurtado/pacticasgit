﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace dotnetcore {
    public class ContactManager :IContacManager {
        List<Contact> contacts { get; set; }
        public Contact GetContact(int ID) {
            Contact result = contacts.Where(t => t.ID == ID).FirstOrDefault();
            return result;
        }
        public void LoadContacs(List<Contact> contacts) {
            this.contacts = contacts;
        }

        public Contact GetLastContact() {
            Contact result = contacts.LastOrDefault();
            return result;
        }
    }
}
